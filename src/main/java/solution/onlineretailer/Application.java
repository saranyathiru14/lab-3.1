package solution.onlineretailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;


@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Application.class, args);

        CartServiceImpl service = context.getBean(CartServiceImpl.class);

        System.out.printf("Welcome to Acme retailers (email: %s)\n", service.getContactEmail());

        //check all the methods work
        // Buy an iphone 13
        service.addItemToCart(0, 1);

        // Buy Coffee Machine
        service.addItemToCart(2, 1);

        // Buy 5 bags of coffee beans
        service.addItemToCart(4, 1);
        service.addItemToCart(4, 4);

        // Remove one bag of coffee
        service.removeItemFromCart(4);

        // Get total cost of items in basket.
        double totalCost = service.calculateCartCost();

        double totalInclusive = service.calculateCartCost() + service.calculateDeliveryCharge() + service.calculateSalesTax();



        //Calculate breakdown of price
        System.out.printf("Items Total: £%.2f", totalCost);
        System.out.printf("\nSales tax: £%.2f", service.calculateSalesTax());
        System.out.printf("\nDelivery Charge: £%.2f", service.calculateDeliveryCharge());
        if(service.calculateFreeDeliveryDiff() == 0) {
            System.out.print("\nCongratulations you qualify for Free delivery!");
        }
        if (service.calculateFreeDeliveryDiff() != 0) {
            System.out.printf("\nSpend £%.2f more to get free deliver! ",service.calculateFreeDeliveryDiff() );
        }


        //calculate total
        System.out.printf("\nTotal Cart cost including Delivery and Tax: £%.2f", totalInclusive );
    }

    @Bean
    public Map<Integer, Item> catalog() {
        Map<Integer, Item> items = new HashMap<>();
        items.put(0, new Item(0, "Apple iphone 13", 999.99));
        items.put(1, new Item(1, "Portafilter", 15.99));
        items.put(2, new Item(2, "LeLit Coffee Machine", 3200));
        items.put(3, new Item(3, "Wireless Mouse", 10.50));
        items.put(4, new Item(4, "Bag of Coffee", 10));
        return items;
    }
}
