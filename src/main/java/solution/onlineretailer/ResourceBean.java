package solution.onlineretailer;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix= "resource")
public class ResourceBean {



    //initialise variables  as private attributes
    private String database;
    private String logs;
    private boolean secure;

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }


    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    @Override
    public String toString() {
        return String.format("resources.database: %s, resources.logs: %s, resources.secure: %s", database, logs, secure);
    }

}
