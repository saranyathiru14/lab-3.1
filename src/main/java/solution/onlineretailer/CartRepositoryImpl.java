package solution.onlineretailer;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CartRepositoryImpl implements CartRepository {

	// Key = ID, Value = Quantity
	private Map<Integer, Integer> cart = new HashMap<>();
	
	@Override
	public void add(int itemId, int quantity) {
		//if existing item in cart, dont add new but update quantity
		Integer existingQuantity = cart.get(itemId);
		if (existingQuantity != null) {
			quantity += existingQuantity;			
		}
		//update hashmap with new quantity if there are no existing items in cart
		cart.put(itemId,  quantity);
	}
	
	@Override
	public void remove(int itemId) {
		cart.remove(itemId);
	}
	
	@Override
	public Map<Integer, Integer> getAll() {
		return cart;
	}
}
